
# Specify node version and choose image
FROM node:18-alpine

# Specify our working directory, this is in our container/in our image
WORKDIR /usr/src/app

# Copy the package.jsons from host to container
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# install all the deps
RUN npm install

# Bundle app source / copy all other files
COPY . .