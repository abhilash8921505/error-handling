import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
  timestamps: true,
})
@ObjectType()
export class User {
  @Field(() => Int)
  @Prop()
  id: number;
}

export type UserDocument = User & Document;

export const UserSchema = SchemaFactory.createForClass(User);
