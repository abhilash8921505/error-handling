import { Field, InputType, Int } from '@nestjs/graphql';
import { IsInt, IsPositive } from 'class-validator';

@InputType()
export class CreateUserDto {
  @Field(() => Int)
  @IsPositive()
  @IsInt()
  id: number;
}
