import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './models/user.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  async create(input: CreateUserDto) {
    const user = new this.userModel(input);
    return await user.save();
  }

  async findOne(id: number) {
    const user = await this.userModel.findOne({ id });
    console.log(user);
    return user;
  }

  findAll() {
    // throw error
    throw new Error('DB error');
    return [];
  }
}
