import { Args, Query, Resolver, Int, Mutation } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './models/user.model';
import { HttpException, HttpStatus } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';

@Resolver()
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Mutation(() => User)
  async create(@Args('input') input: CreateUserDto) {
    const user = this.usersService.create(input);
    return user;
  }

  @Query(() => User)
  findOne(@Args('id', { type: () => Int }) id: number) {
    const user = this.usersService.findOne(id);
    return user;
  }

  @Query(() => [User])
  findAll() {
    try {
      const users = this.usersService.findAll();
      return users;
    } catch (error) {
      throw new HttpException(
        'Internal Server Error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
