# Error Handling in Nest & GraphQL

## Observations

1. Apollo GraphQL deals with error in its own way. In REST, we send HTTP level status codes for response. But, in GraphQL all response have 200 OK status code. Errors/Status Code info will be there in the errors field of the response body.
2. In Apollo GraphQL, `includeStacktraceInErrorResponses` options defaults to `true` to unless NODE_ENV is production or test. Instead of relying on what they have set as default, which might change in future versions, we should set that option ourselves
3. Handle, `null` scenarios properly. For example => `findOne` query will throw an error if its response is non-nullable in Schema. This issue can be dealt in 2 ways => Throwing DB error on repo level or To allow the set the response to nullable.
4.

## Errors

### Different types of Errors

Resources: [link1](https://www.apollographql.com/blog/graphql/error-handling/full-stack-error-handling-with-graphql-apollo/)

#### A developer’s view of errors

Generally speaking, there are two dimensions along which we can categorize errors.

1. Is it the client or server that is at fault?

   - **Parse Phase**: Client sent malformed GraphQL request, i.e. syntax error
   - **Validation Phase**: Client sent inputs that failed GraphQL type checking
   - **Execution Phase**: Client sent bad user input (request out of range, resource not found, duplicate key) or had bad permissions (user is not authorized to perform some action).

1. Where did the error occur?

   - **networkError**: Errors that are thrown outside of your resolvers. If networkError is present in your response, it means your entire query was rejected, and therefore no data was returned. For example, the client failed to connect to your GraphQL endpoint, or some error occurred within your request middleware, or there was an error in the parse/validation phase of your query.
   - **graphQLErrors**: Any error that is thrown within your resolvers. Since we have multiple resolvers executing that are all potential sources of errors, we need an array to represent all of them. More importantly, graphQLErrors from failed resolvers can be returned alongside data/fields that resolved successfully.

##### GraphQL Errors

###### Returning null for non-nullable fields

I am trying to query a non-existent id. Mongoose `findOne` returns `null` when non-matching query. GQL throws when it sees that response is `null` while schema doesn't allow output to be nullable

```graphql
query {
  findOne(id: 4) {
    id
  }
}
```

```json
{
  "errors": [
    {
      "message": "Cannot return null for non-nullable field Query.findOne.",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": ["findOne"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR"
      }
    }
  ],
  "data": null
}
```
